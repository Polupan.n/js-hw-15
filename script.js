"use strict";

// Теоритичні питання:
// 1. В чому відмінність між setInterval та setTimeout?

// setInterval використовується для повторення виконання коду через визначені часові інтервали. Код, переданий у setInterval, буде виконуватися знову і знову, поки його явно не буде скасовано за допомогою clearInterval.

// setTimeout використовується для запуску коду один раз через визначений проміжок часу. Код, переданий у setTimeout, виконається лише один раз після вказаної затримки.

// 2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали?

// Ні, неможливо гарантувати абсолютну точність виконання функцій в setInterval та setTimeout через вказаний проміжок часу. Обидва методи залежать від того, як потік виконання коду обробляється в середовищі виконання JavaScript.
// В більшості випадків код буде виконаний приблизно через вказаний проміжок часу, фактичний час виконання може змінюватися через різні фактори, такі як завантаженість системи, наявність інших виконуючихся задач, та інші обставини.

// 3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

// Для припинення виконання функцій setTimeout та setInterval треба викрнати функцію clearTimeout або clearInterval відповідно.

// Практичне завдання 1:

// -Створіть HTML-файл із кнопкою та елементом div.
// -При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.

const btn = document.getElementById(`btn`);
const div = document.querySelector(`div`);
btn.style.cssText = "background-color: gray; color: #fff; padding: 20px";
btn.textContent = `Запусти таймер!`;

btn.addEventListener(`click`, () => {
  setTimeout(() => {
    div.textContent = `Операція виконана успішно!`;
  }, 3000);
});

// Практичне завдання 2:

// Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. Після досягнення 1 змініть текст на "Зворотній відлік завершено".

document.addEventListener("DOMContentLoaded", () => {
  const countdownElement = document.getElementById("countdown");
  let seconds = 10;
  function updateCountdown() {
    countdownElement.textContent = seconds;
    if (seconds === 1) {
      clearInterval(timer);
      countdownElement.textContent = "Зворотній відлік завершено";
    }
    seconds--;
  }
  const timer = setInterval(updateCountdown, 1000);
});
